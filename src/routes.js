import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MovieList from './pages/MovieList';
import MovieDetails from './pages/MovieDetails';

export default createAppContainer(
    createSwitchNavigator({
        MovieList,
        MovieDetails
    })
)