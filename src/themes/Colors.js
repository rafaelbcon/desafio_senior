const colors = {
  white: '#ffffff',
  lightBlue: '#5ca0d3',
  gray: '#a1a1a1',
  darkGray: '#2c3848',
}

export default colors
