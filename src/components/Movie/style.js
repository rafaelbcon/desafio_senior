import { StyleSheet } from 'react-native';
import Colors from '~/themes/Colors';

export default StyleSheet.create({
  movie: {
    marginRight: 20,
    width: 130
  },
  moviePoster: {
    width: 130, 
    height: 170,
    borderRadius: 20
  },
  movieTitle: {
    marginTop: 13,
    marginBottom: 6,
    fontWeight: 'bold',
    flex: 1,
    flexWrap: 'wrap',
    color: Colors.white,
    fontSize: 13
  },
  movieStars: {
    fontWeight: 'bold',
    color: Colors.white,
    fontSize: 16
  },
  starStyle: {
    width: 100,
    height: 20,
    marginBottom: 20,
  }
});