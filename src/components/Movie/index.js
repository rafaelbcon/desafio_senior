import React from 'react';
import { TouchableOpacity, View, Text, Image } from 'react-native';
import styles from './style';

export default function Movie ({ movie, navigation }) {
  return (
    <TouchableOpacity onPress={ () => {
      navigation.navigate('MovieDetails', {
        movie: movie
      })
    } }>
      <View style={ styles.movie }>
        <Image 
          style={ styles.moviePoster }
          source={{ uri: "http://image.tmdb.org/t/p/w185/" + movie.poster_path }} />

        <Text style={ styles.movieTitle }>{ movie.title }</Text>
      </View>
    </TouchableOpacity>
  );
}