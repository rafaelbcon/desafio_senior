import React from 'react';
import { View, Text, TouchableOpacity, ScrollView } from 'react-native';

import styles from './style';

export default function MoviesList({children, title, seemore, ...rest}) {
  return (
    <View style={styles.content} { ...rest }>
      <View style={styles.contentHeader}>
        <Text style={styles.contentTitle}>{ title }</Text>
        <TouchableOpacity
          hitSlop={{top: 20, bottom: 20, left: 20, right: 20}}
          onPress={() => {
            alert('Em desenvolvimento');
          }}>
          <Text style={styles.contentViewMore}>{ seemore }</Text>
        </TouchableOpacity>
      </View>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        style={styles.scrollMovies}>
        { children }
      </ScrollView>
    </View>
  );
}
