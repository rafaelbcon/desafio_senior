import { StyleSheet } from 'react-native';
import Colors from '~/themes/Colors';

export default StyleSheet.create({
  content: {
    paddingBottom: 24,
    paddingHorizontal: 30
  },
  scrollMovies: {},
  contentHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 24
  },  
  contentTitle: {
    fontWeight: 'bold',
    color: Colors.white,
    fontSize: 13
  },
  contentViewMore: {
    marginVertical: 10,
    color: Colors.gray,
    fontSize: 13
  },
});