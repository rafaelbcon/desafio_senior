import produce from 'immer';

import * as MovieTypes from './types';

const INITIAL_STATE = {
    nowPlayingData: null,
    nowPlayingError: null,
    nowPlayingLoading: false,

    popularData: null,
    popularError: null,
    popularLoading: false,

    topRatedData: null,
    topRatedError: null,
    topRatedLoading: false,

    upcomingData: null,
    upcomingError: null,
    upcomingLoading: false,

    movieDate: null,
    movieError: null,
    movieLoading: false,

    search: ''
};

export default function moviesReducer (state = INITIAL_STATE, action) {
    return produce(state, draft => {
        switch (action.type) {
            case MovieTypes.GET_NOW_PLAYING_MOVIES_REQUEST: {
                draft.nowPlayingLoading = true;
                break;
            }

            case MovieTypes.GET_NOW_PLAYING_MOVIES_SUCCESS: {
                draft.nowPlayingData = action.payload;
                draft.nowPlayingError = null;
                draft.nowPlayingLoading = false;
                break;
            }

            case MovieTypes.GET_NOW_PLAYING_MOVIES_FAILURE: {
                draft.nowPlayingData = null;
                draft.nowPlayingError = action.payload;
                draft.nowPlayingLoading = false;
                break;
            }

            case MovieTypes.GET_POPULAR_MOVIES_REQUEST: {
                draft.popularLoading = true;
                break;
            }

            case MovieTypes.GET_POPULAR_MOVIES_SUCCESS: {
                draft.popularData = action.payload;
                draft.popularError = null;
                draft.popularLoading = false;
                break;
            }

            case MovieTypes.GET_POPULAR_MOVIES_FAILURE: {
                draft.popularData = null;
                draft.popularError = action.payload;
                draft.popularLoading = false;
                break;
            }

            case MovieTypes.GET_TOP_RATED_MOVIES_REQUEST: {
                draft.topRatedError = true;
                break;
            }

            case MovieTypes.GET_TOP_RATED_MOVIES_SUCCESS: {
                draft.topRatedData = action.payload;
                draft.topRatedError = null;
                draft.topRatedError = false;
                break;
            }

            case MovieTypes.GET_TOP_RATED_MOVIES_FAILURE: {
                draft.topRatedData = null;
                draft.topRatedError = action.payload;
                draft.topRatedError = false;
                break;
            }

            case MovieTypes.GET_UPCOMING_MOVIES_REQUEST: {
                draft.upcomingLoading = true;
                break;
            }

            case MovieTypes.GET_UPCOMING_MOVIES_SUCCESS: {
                draft.upcomingData = action.payload;
                draft.upcomingError = null;
                draft.upcomingLoading = false;
                break;
            }

            case MovieTypes.GET_UPCOMING_MOVIES_FAILURE: {
                draft.upcomingData = null;
                draft.upcomingError = action.payload;
                draft.upcomingLoading = false;
                break;
            }

            case MovieTypes.GET_MOVIE_REQUEST: {
                draft.movieLoading = true;
                break;
            }

            case MovieTypes.GET_MOVIE_SUCCESS: {
                draft.movieData = action.payload;
                draft.movieError = null;
                draft.movieLoading = false;
                break;
            }

            case MovieTypes.GET_MOVIE_FAILURE: {
                draft.movieData = null;
                draft.movieError = action.payload;
                draft.movieLoading = false;
                break;
            }

            case MovieTypes.GET_SEARCH_MOVIE: {
                draft.search = action.payload;
                break;
            }
        }
    });
}