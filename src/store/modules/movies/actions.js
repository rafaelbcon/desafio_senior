import * as MovieTypes from './types';

export function getNowPlayingMoviesRequest() {
    return {
        type: MovieTypes.GET_NOW_PLAYING_MOVIES_REQUEST
    }
}

export function getNowPlayingMoviesSuccess(data) {
    return {
        type: MovieTypes.GET_NOW_PLAYING_MOVIES_SUCCESS,
        payload: data
    }
}

export function getNowPlayingMoviesFailure(data) {
    return {
        type: MovieTypes.GET_POPULAR_MOVIES_FAILURE,
        payload: data
    }
}

export function getPopularMoviesRequest() {
    return {
        type: MovieTypes.GET_POPULAR_MOVIES_REQUEST
    }
}

export function getPopularMoviesSuccess(data) {
    return {
        type: MovieTypes.GET_POPULAR_MOVIES_SUCCESS,
        payload: data
    }
}

export function getPopularMoviesFailure(data) {
    return {
        type: MovieTypes.GET_POPULAR_MOVIES_FAILURE,
        payload: data
    }
}

export function getTopRatedMoviesRequest() {
    return {
        type: MovieTypes.GET_TOP_RATED_MOVIES_REQUEST
    }
}

export function getTopRatedMoviesSuccess(data) {
    return {
        type: MovieTypes.GET_TOP_RATED_MOVIES_SUCCESS,
        payload: data
    }
}

export function getTopRatedMoviesFailure(data) {
    return {
        type: MovieTypes.GET_TOP_RATED_MOVIES_FAILURE,
        payload: data
    }
}

export function getUpcomingMoviesRequest() {
    return {
        type: MovieTypes.GET_UPCOMING_MOVIES_REQUEST
    }
}

export function getUpcomingMoviesSuccess(data) {
    return {
        type: MovieTypes.GET_UPCOMING_MOVIES_SUCCESS,
        payload: data
    }
}

export function getUpcomingMoviesFailure(data) {
    return {
        type: MovieTypes.GET_UPCOMING_MOVIES_FAILURE,
        payload: data
    }
}

export function getMovieRequest() {
    return {
        type: MovieTypes.GET_MOVIE_REQUEST
    }
}

export function getMovieSuccess(data) {
    return {
        type: MovieTypes.GET_MOVIE_SUCCESS,
        payload: data
    }
}

export function getMovieFailure(data) {
    return {
        type: MovieTypes.GET_MOVIE_FAILURE,
        payload: data
    }
}

export function getSearchMovie(data) {
    return {
        type: MovieTypes.GET_SEARCH_MOVIE,
        payload: data
    }
}