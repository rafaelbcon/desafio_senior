import { takeLatest, call, put, all } from 'redux-saga/effects';

import api from '~/services/api';

import * as MovieActions from './actions';
import * as MovieTypes from './types';

const api_key = '?api_key=2818fe42b4a9cf2896435f369f4bf66e';

export function* getNowPlayingMovies ({ payload }) {
    try {
        const response = yield call(api.get, '/3/movie/now_playing' + api_key);

        yield put(MovieActions.getNowPlayingMoviesSuccess(response.data));
    } catch (err) {
        yield put(MovieActions.getNowPlayingMoviesFailure(err.response.data));
    }
}

export function* getPopularMovies ({ payload }) {
    try {
        const response = yield call(api.get, '/3/movie/popular' + api_key);

        yield put(MovieActions.getPopularMoviesSuccess(response.data));
    } catch (err) {
        yield put(MovieActions.getPopularMoviesFailure(err.response.data));
    }
}

export function* getTopRatedMovies ({ payload }) {
    try {
        const response = yield call(api.get, '/3/movie/top_rated' + api_key);

        yield put(MovieActions.getTopRatedMoviesSuccess(response.data));
    } catch (err) {
        yield put(MovieActions.getTopRatedMoviesFailure(err.response.data));
    }
}

export function* getUpcomingMovies ({ payload }) {
    try {
        const response = yield call(api.get, '/3/movie/upcoming' + api_key);

        yield put(MovieActions.getUpcomingMoviesSuccess(response.data));
    } catch (err) {
        yield put(MovieActions.getUpcomingMoviesFailure(err.response.data));
    }
}

export function* getMovie ({ payload }) {
    try {
        console.tron.logImportant(payload);
        // const response = yield call(api.get, '/3/movie/' + payload.action + api_key);

        // yield put(MovieActions.getMovieSuccess(response.data));
    } catch (err) {
        // yield put(MovieActions.getMovieFailure(err.response.data));
    }
}

export default all([
    takeLatest(MovieTypes.GET_NOW_PLAYING_MOVIES_REQUEST, getNowPlayingMovies),
    takeLatest(MovieTypes.GET_POPULAR_MOVIES_REQUEST, getPopularMovies),
    takeLatest(MovieTypes.GET_TOP_RATED_MOVIES_REQUEST, getTopRatedMovies),
    takeLatest(MovieTypes.GET_UPCOMING_MOVIES_REQUEST, getUpcomingMovies),
    takeLatest(MovieTypes.GET_MOVIE_REQUEST, getMovie),
]) 