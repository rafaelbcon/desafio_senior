import React, { Component } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';

import styles from './style';
import {Icon} from 'react-native-elements';

import { AirbnbRating } from 'react-native-ratings';
import _ from 'lodash';

export default class MovieDetails extends Component {

  state = {
    movie: {}
  }

  componentDidMount () {
    this.setState({
      movie: this.props.navigation.getParam('movie')
    })
  }

  render () {

    const { movie } = this.state;

    return (
      <>
        <ImageBackground
          style={ styles.imageBackground }
          source={{
            uri: 'http://image.tmdb.org/t/p/original/' + _.get(movie, 'backdrop_path')
          }}>
          <SafeAreaView>
            <View
              style={ styles.header }>
              <TouchableOpacity onPress={() => this.props.navigation.navigate('MovieList')}>
                <Icon name="arrow-left" type="font-awesome" color="#ffffff" />
              </TouchableOpacity>
              <TouchableOpacity>
                <Icon name="heart" type="font-awesome" color="#ffffff" />
              </TouchableOpacity>
            </View>
          </SafeAreaView>
        </ImageBackground>
        <ScrollView style={styles.container}>
          <View style={styles.content}>
            <View
              style={ styles.titleContent }>
              <Text
                style={ styles.titleText }>
                { _.get(movie, 'title', 'title') }
              </Text>
              <Icon name="search" size={30} color="#ffffff" />
            </View>
  
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
              }}>
              <TouchableOpacity
                style={ styles.watchNowContent }
                onPress={() => {
                  alert('Em desenvolvimento');
                }}>
                <Text
                  style={ styles.watchNowText }>
                  WATCH NOW
                </Text>
              </TouchableOpacity>
              <Text style={{color: '#ffffff'}}>
              <AirbnbRating
                count={10}
                defaultRating={ _.get(movie, 'vote_average', 0)}
                size={10}
                style={{
                  paddingTop: 0,
                  marginTop: 0
                }}
              />
              </Text>
            </View>
  
            <View>
              <Text style={styles.contentText}>
                { _.get(movie, 'overview', 'title') }
              </Text>
            </View>
  
            <View>
              <View style={styles.info}>
                <Text style={styles.infoTitle}>Studio</Text>
                <Text style={styles.infoContent}>Warner Bros.</Text>
              </View>
              <View style={styles.info}>
                <Text style={styles.infoTitle}>Genre</Text>
                <Text style={styles.infoContent}>Action, Adventure, Fantasy</Text>
              </View>
              <View style={styles.info}>
                <Text style={styles.infoTitle}>Release</Text>
                <Text style={styles.infoContent}>{ _.get(movie, 'release_date', 'release_date') }</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      </>
    );
  }
}
