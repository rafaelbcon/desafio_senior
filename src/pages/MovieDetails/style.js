import { StyleSheet } from 'react-native';
import Colors from '~/themes/Colors';

export default StyleSheet.create({
  imageBackground: {
    width: '100%',
    height: 320,
  },
  header: {
    paddingVertical: 15,
    paddingHorizontal: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  titleContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 20,
  },
  titleText: {
    fontSize: 32,
    fontWeight: 'bold',
    color: Colors.white
  },
  watchNowContent: {
    borderRadius: 20,
    paddingVertical: 10,
    paddingHorizontal: 20,
    backgroundColor: '#cccccc',
  },
  watchNowText: {
    color: '#ffffff',
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    backgroundColor: Colors.darkGray,
  },
  content: {
    paddingHorizontal: 30,
  },
  contentText: {
    marginVertical: 25,
    lineHeight: 25,
    fontSize: 16,
    color: Colors.white,
  },
  info: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 5,
    color: Colors.white,
  },
  infoTitle: {
    width: 80,
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.white,
  },
  infoContent: {
    fontSize: 16,
    color: Colors.white,
  },
});