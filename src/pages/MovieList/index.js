import React, {Component} from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  Text,
  TextInput,
  ActivityIndicator,
} from 'react-native';

import {connect} from 'react-redux';
import {
  getNowPlayingMoviesRequest,
  getPopularMoviesRequest,
  getTopRatedMoviesRequest,
  getUpcomingMoviesRequest,
  getSearchMovie,
} from '~/store/modules/movies/actions';

import MoviesList from '~/components/MovieList';
import Movie from '~/components/Movie';

import { Icon } from 'react-native-elements';
import styles from './style';

class MovieList extends Component {

  componentDidMount() {
    this.props.getNowPlayingMoviesRequest();
    this.props.getPopularMoviesRequest();
    this.props.getTopRatedMoviesRequest();
    this.props.getUpcomingMoviesRequest();
  }

  render() {
    const {
      nowPlayingData,
      popularData,
      topRatedData,
      upcomingData,
      nowPlayingLoading,
      popularLoading,
      topRatedLoading,
      upcomingLoading
    } = this.props;

    return (
      <>
        <SafeAreaView style={styles.scrollview}>
          <View style={styles.header}>
            <Text style={styles.headerTitle}>Hello, what do you</Text>
            <Text style={styles.headerTitle}>want to watch?</Text>

            <View style={styles.searchForm}>
              <View style={styles.searchIcon}>
                <Icon
                  name='search'
                  type='font-awesome'
                  color="#ffffff"
                />
              </View>
              <TextInput
                style={styles.searchInput}
                autoCorrect={false}
                autoCapitalize="none"
                placeholder="Search"
                placeholderTextColor="#ffffff"
                value={this.props.search}
                onChangeText={search => this.props.getSearchMovie(search)}
              />
            </View>
          </View>
        </SafeAreaView>
        <View style={styles.container}>
          <ScrollView>
            {this.props.search.length > 0 ? (
              <View style={{
                flex: 1, 
                marginHorizontal: 30
              }}>
                <Text style={{
                  fontSize: 24,
                  color: '#ffffff'
                }}>Em desenvolvimento</Text>
              </View>
            ) : (
              <>
                <MoviesList title="NOW PLAYING" seemore="See all">
                  {nowPlayingLoading ? (
                    <ActivityIndicator size="large" color="#ffffff" />
                  ) : (
                    nowPlayingData &&
                    nowPlayingData.results.map(movie => (
                      <Movie movie={movie} navigation={this.props.navigation} />
                    ))
                  )}
                </MoviesList>

                <MoviesList title="POPULAR" seemore="See all">
                  {popularLoading ? (
                    <ActivityIndicator size="large" color="#ffffff" />
                  ) : (
                    popularData &&
                    popularData.results.map(movie => 
                      <Movie movie={movie} navigation={this.props.navigation} />
                    )
                  )}
                </MoviesList>

                <MoviesList title="RECOMMENDED FOR YOU" seemore="See all">
                  {topRatedLoading ? (
                    <ActivityIndicator size="large" color="#ffffff" />
                  ) : (
                    topRatedData &&
                    topRatedData.results.map(movie => 
                      <Movie movie={movie} navigation={this.props.navigation} />
                    )
                  )}
                </MoviesList>

                <MoviesList title="RECOMMENDED FOR YOU" seemore="See all">
                  {upcomingLoading ? (
                    <ActivityIndicator size="large" color="#ffffff" />
                  ) : (
                    upcomingData &&
                    upcomingData.results.map(movie => 
                      <Movie movie={movie} navigation={this.props.navigation} />
                    )
                  )}
                </MoviesList>
              </>
            )}
            <View style={{height: 50}} />
          </ScrollView>
        </View>
      </>
    );
  }
}

const mapStateToProps = state => ({
  nowPlayingData: state.moviesReducer.nowPlayingData,
  nowPlayingLoading: state.moviesReducer.nowPlayingLoading,
  popularData: state.moviesReducer.popularData,
  popularLoading: state.moviesReducer.popularLoading,
  topRatedData: state.moviesReducer.topRatedData,
  topRatedLoading: state.moviesReducer.topRatedLoading,
  upcomingData: state.moviesReducer.upcomingData,
  upcomingLoading: state.moviesReducer.upcomingLoading,
  search: state.moviesReducer.search,
});

export default connect(mapStateToProps, {
  getNowPlayingMoviesRequest,
  getPopularMoviesRequest,
  getTopRatedMoviesRequest,
  getUpcomingMoviesRequest,
  getSearchMovie,
})(MovieList);
