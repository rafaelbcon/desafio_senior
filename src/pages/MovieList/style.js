import { StyleSheet } from 'react-native';
import Colors from '~/themes/Colors';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2c3848',
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    marginTop: -50,
    paddingTop: 30,
  },
  scrollview: {
    backgroundColor: Colors.lightBlue,
  },
  header: {
    backgroundColor: Colors.lightBlue,
    height: 244,
    paddingTop: 10,
    paddingLeft: 40,
    paddingRight: 40,
  },
  headerTitle: {
    fontSize: 26,
    fontWeight: 'bold',
    color: Colors.white,
  },
  searchForm: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
    borderRadius: 50,
    marginTop: 30,
  },
  searchIcon: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  searchInput: {
    color: '#ffffff',
    paddingLeft: 10,
    fontSize: 16,
  },
});